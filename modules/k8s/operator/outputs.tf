output "namespace" {
  value = local.namespace
}

output "operator_version" {
  value = local.operator_version
}