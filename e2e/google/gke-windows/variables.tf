
variable "gitlab_project_id" {
  type = string
}

variable "name" {
  type = string
}

variable "runner_tag" {
  type = string
}
